package idhash.main;

/**
 * This class includes most basic methods like filtering hash and salt from
 * input.
 *
 * @version 02.02.2012
 * @author agilob
 */
public class Regexper {

    /**
     * Method filters hash from input string.
     *
     * @param string
     */
    public String getHash(String string) {
        int colom = 0;
        colom = string.indexOf(':');

        if (colom == -1) {
            colom = string.length();
        }

        String hash = string.substring(0, colom);
        return hash;
    } // getHash

    /**
     * Method filters salt from input if possible for salt to exist.
     */
    public String getSalt(String string) {

        String salt;
        int colom = 0;
        colom = string.indexOf(':') + 1; //+1 because of the colom
        salt = string.substring(colom, string.length());
        System.out.println(salt.length());
        return salt;
    } // getSalt

    /**
     * Method returns length of a salt if salt exists.
     */
    public int saltLength(String string) {

        int length = string.length();
        System.out.println(string.length());

        return length;
    } // saltLength
}