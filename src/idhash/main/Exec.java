package idhash.main;

/**
 * This is main executable class. Class is responsible for admeasurement string
 * to a specified hash kind.
 *
 * @see Regexper
 * @see Md5
 * @see Sha
 * @See Others
 *
 * @version 13.02.2012
 *
 * @author agilob
 */
import idhash.hash.Md5;
import idhash.hash.Others;
import idhash.hash.Sha;
import java.util.ArrayList;
import java.util.List;

public class Exec {

    static Regexper reg = new Regexper();
    static Md5 md5check = new Md5();
    static Sha shacheck = new Sha();
    static Others others = new Others();
    //static Gui frame = new Gui();
    
    public String dataFromInput = "";
    
    public static List<String> results = new ArrayList<String>();

    public void run() {


        boolean md5 = false;
        boolean sha = false;
        boolean oth = false; //other kid of hash


        String mainSalt = "";
        String mainHash = "";

        /**
         * Filtered hash from whole input.
         */
        mainHash = reg.getHash(dataFromInput);

        /**
         * Important for if-conditional. Checking between md5/sha/other class.
         */
        int mainHashLength;

        /**
         * Important to choose hash kind when algorithm uses salt.
         */
        int mainSaltLength;

        /**
         * Value is important do decide which kind of hash should be checked.
         */
        mainHashLength = mainHash.length();

        if (mainHashLength == 32 || mainHashLength == 34) {
            md5 = true;

            if (dataFromInput.length() > 34) {
                mainSalt = reg.getSalt(dataFromInput);
                mainSaltLength = mainSalt.length();
            }

        } //if mainHashLength

        if (mainHashLength == 40) {
            sha = true;
            oth = true;
        } //if mainHashLength
        else {
            oth = true; //for shorter hash, like crc
        }
        if (md5 == true) {
            md5check.check(mainHash, mainSalt);
        } //if md5

        if (oth == true) {
            others.checkit(mainHash, dataFromInput);
        } //if oth

        if (sha == true) {
            shacheck.checkit(mainHash, dataFromInput);
        } //if sha

        if (md5 == false && sha == false && oth == false) {
            results.add("Sorry... nothing. Chech yours' hash syntax.");
        }
    } //main void
}